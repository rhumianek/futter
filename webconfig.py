import bottle, bottle_sqlite,sqlite3
from bottle import run, template, route, response, request
from json import dumps
app = bottle.Bottle()
plugin = bottle_sqlite.Plugin(dbfile='usables/trade.db')
app.install(plugin)

def dict_factory(cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d


@app.route('/resources')
def showresources(db):
    searchterm = request.query.term
    conn = sqlite3.connect('usables/trade.db')
    conn.row_factory = dict_factory
    cursor = conn.cursor()
    result = cursor.execute("select  coalesce(l ,' ') || ' ' || coalesce(f, ' ') || ' '|| coalesce(c, ' ')  as `value`, id as `id` from players where (l like ? or f like ? or c like ?)", ('%{}%'.format(searchterm),'%{}%'.format(searchterm),'%{}%'.format(searchterm))).fetchall()
    conn.close()
    if result:
        response.content_type = 'application/json'
        return dumps(result)
    else:
        return q

@app.route('/players')
def showaassets(db):
    result = db.execute('SELECT * from bidding').fetchall()
    if result:
        a = template('templates/players', rows=result)
        return a
    return HTTPError(404, "Page not found")

@app.route('/bidlist')
def managerequests():
    if request.query.delid:
        del_id = request.query.delid
        conn = sqlite3.connect('usables/trade.db')
        cursor = conn.cursor()
        delitems = ('{}'.format(del_id),)
        cursor.execute("delete from bidding where asset_id = ?",delitems)
        conn.commit()
        conn.close()
        pass
    elif request.query.addid:
        asset_id = request.query.addid
        name = request.query.name
        maxbid = request.query.maxbid
        maxbin = request.query.maxbin
        sellmin = request.query.sellmin
        sellmax = request.query.sellmax
        conn = sqlite3.connect('usables/trade.db')
        cursor = conn.cursor()
        additems = (asset_id, '{}'.format(name), maxbid, maxbin, sellmin, sellmax)
        cursor.execute("insert into bidding values(?,?,?,?,?,?)", additems)
        conn.commit()
        conn.close()
    else:
        return None

@app.route('/')
def test():
    a = template('templates/addtobidlist')
    return a


app.run()
#run(host='localhost', port=8080)