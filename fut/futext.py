import fut,time,collections, sys, sqlite3, random
from termcolor import colored
# TODO znalezc dlaczego robia sie przerwy w logu
class Futext(fut.Core):

    def playerinfo(self,id):
        p = fut.core.players()
        n = fut.core.nations()
        if id in p:
            lastname = p[id]['lastname']
            firstname = p[id]['firstname']
            ovr = p[id]['rating']
            nation = n[p[id]['nationality']]
            l = [lastname,firstname,ovr,nation]
            return l
        else:
            raise ValueError('This is not a valid player resource')

    def randomtime(self, mint, maxt):
        t = random.triangular(mint, maxt)
        time.sleep(t)

    @property
    def printabletradepile(self):
        tpile = self.tradepile()
        #players = self.players()
        text =''
        requests = 0
        i = 0
        while i <len(tpile):
            resId = tpile[i]['resourceId']
            try:
                p = self.playerinfo(resId)
                if tpile[i]['tradeState']== 'closed':
                    text += colored('[Sold   ] {} {} (OVR:{} NAT:{}) (BID:{} CUR:{} BIN:{} STATUS:{})\n'.format(p[0],p[1],p[2],p[3],tpile[i]['startingBid'], tpile[i]['currentBid'],tpile[i]['buyNowPrice'],tpile[i]['tradeState']),color='green')
                    if self.tradepileDelete(tpile[i]['tradeId']):
                        text += colored('[Watcher] Removed sold {} {} (OVR:{} NAT:{}) (BID:{} CUR:{} BIN:{} STATUS:{})\n'.format(p[0],p[1],p[2],p[3],tpile[i]['startingBid'], tpile[i]['currentBid'],tpile[i]['buyNowPrice'],tpile[i]['tradeState']))
                else:
                    text += '[Listed ] {} {} (OVR:{} NAT:{}) (BID:{} CUR:{} BIN:{} STATUS:{})\n'.format(p[0],p[1],p[2],p[3],tpile[i]['startingBid'], tpile[i]['currentBid'],tpile[i]['buyNowPrice'],tpile[i]['tradeState'])
            except ValueError:
                text += colored('[Error  ] It is a consumable or special card\n',color='red',on_color='on_yellow')
            i +=1
            #result = collections.namedtuple('result', ['log','requests'])(text, requests)
        return text

    def printablecredits(self, startingcredits=None): 
        if startingcredits:
            coins = self.credits
            zysk = self.credits - startingcredits
            text = colored('[Coins  ] {} Balance from start: {}\n'.format(coins, zysk),'yellow',on_color=None)
        else:
            text = colored('[Coins  ] {}\n'.format(self.credits),'yellow',on_color=None)
        return text

    @property
    def relistwithlog(self):
        self.relist()
        return self.printabletradepile

    @property
    def silverbiding(self):
        #biding rare silver player cards from Premier League up to 300 coins (GK excluded)
        requests = 0
        for x in range (3,5):# szukaj od strony 1 do 5
            auctionsList = self.searchAuctions(ctype='player',level='gold', league=13, max_price=300, start=x)
            #auctionsList = self.searchAuctions(ctype='player', league=13, level='silver', max_price=300, start=x)
            #print(auctionsList)
            #time.sleep(2)
            i = 0
            text = ''
            requests += 1
            while i < len(auctionsList):
                if auctionsList[i]['rareflag'] == 1 and auctionsList[i]['position'] != 'GK':
                    p = self.playerinfo(auctionsList[i]['assetId'])
                    if  self.checklists is True:#jezeli jest więcej niz 1 miejsce na wlist i tpile
                        if self.bid(auctionsList[i]['tradeId'],300,fast=False):
                            text+=colored('[Bider  ] Biding {} {} (OVR:{} NAT:{}) -> tradeId:{}; \n'.format(p[0],p[1],p[2],p[3], auctionsList[i]['tradeId']), color='blue')
                            requests += 1
                        else:
                            text+=colored('[Bider  ] Could not bid {} {} (OVR:{} NAT:{}) -> tradeId:{}; \n'.format(p[0],p[1],p[2],p[3], auctionsList[i]['tradeId']), color='magenta')
                            requests +=1
                        self.randomtime(1,3)
                    else:
                        text+=colored('[Watcher] No space on lists skipping biding')
                else:
                    pass #jak zawodnik nie spelnia kryteriow - nic nie robi ale moze by w przyszlosci chcialo
                i += 1
            result = collections.namedtuple('result',['log','requests'])(text,requests)
        return result

    @property
    def silverwatchlistmanager(self):
        requests = 0
        wlist = self.watchlist()
        requests += 1
        i=0
        text=''
        while i < len(wlist):
            p = self.playerinfo(wlist[i]['assetId'])
            if wlist[i]['bidState'] == 'outbid' and wlist[i]['expires'] == -1: #przebity aukcja zakocznona
                self.watchlistDelete(wlist[i]['tradeId'])
                self.randomtime(1,3)
                requests += 1
            elif wlist[i]['bidState'] == 'outbid' and wlist[i]['expires'] > 1: #przebity aukcja trwa nic nie robi ale moze w przyszlosci...
                pass
            elif wlist[i]['bidState'] == 'highest' and wlist[i]['expires'] == -1: #aukcja wygrana
                text+=colored('[Watcher] Won {} {} (OVR:{} NAT:{}) -> tradeId:{}; \n'.format(p[0],p[1],p[2],p[3], wlist[i]['tradeId']), color='blue')
                if self.sendToTradepile(wlist[i]['id']):
                    text+=colored('[Watcher] Moving to tradepile {} {} (OVR:{} NAT:{}) -> tradeId:{}; \n'.format(p[0],p[1],p[2],p[3], wlist[i]['tradeId']))
                    self.randomtime(1, 3)
                    requests += 1
                    if self.sell(wlist[i]['id'],550,600,duration=3600):
                        text+=colored('[List   ] Auction set {} {} (OVR:{} NAT:{}) -> tradeId:{}; \n'.format(p[0],p[1],p[2],p[3], wlist[i]['tradeId']), color='blue')
            i += 1
        result = collections.namedtuple('result',['log', 'requests'])(text,requests)
        return result

    @property
    def checklists(self): #sprawdza miejsce na listach zwraca True jak sa przynajmniej dwa wolne miejsca
        if self.tradepile_size-len(self.tradepile()) > 1 and self.watchlist_size-len(self.watchlist()) > 1:
            a = True
        else:
            a = False
        return a

    @property
    def waitforwatchlist(self):
        requests = 0
        wlist = self.watchlist()
        requests += 1
        waittime = 0
        i = 0
        text = ''
        while i < len(wlist):
            if wlist[i]['expires'] > waittime:
                waittime = wlist[i]['expires']
            i += 1
        if waittime < 0:
            waittime = 0
        result = collections.namedtuple('result',['waittime', 'requests'])(waittime, requests)
        return result

    def auctioner(self, wlist):#TODO do pobierania z bazy BID I BIN dla zawodnikow i wystawiania aukcji
        requests = 0
        requests += 1
        i=0
        text=''
        p = self.playerinfo(wlist['assetId'])
        if wlist['bidState'] == 'outbid' and wlist['expires'] == -1: #przebity aukcja zakocznona
            self.watchlistDelete(wlist['tradeId'])
            self.randomtime(1, 3)
            requests += 1
        elif wlist['bidState'] == 'outbid' and wlist['expires'] > 1: #przebity aukcja trwa nic nie robi ale moze w przyszlosci...
            pass
        elif wlist['bidState'] == 'highest' and wlist['expires'] == -1: #aukcja wygrana
            text+=colored('[Watcher] Won {} {} (OVR:{} NAT:{}) -> tradeId:{}; \n'.format(p[0],p[1],p[2],p[3], wlist['tradeId']), color='blue')
            if self.sendToTradepile(wlist['id']):
                text+=colored('[Watcher] Moving to tradepile {} {} (OVR:{} NAT:{}) -> tradeId:{}; \n'.format(p[0],p[1],p[2],p[3], wlist['tradeId']))
                self.randomtime(1, 3)
                requests += 1
                conn = sqlite3.connect('usables/trade.db')
                cursor = conn.cursor()
                asset = ('{}'.format(wlist['assetId']),)
                prices = cursor.execute("Select sellmin, sellmax from bidding where asset_id = ? limit 1", asset).fetchone()
                conn.close()
                if self.sell(wlist['id'],prices[0],prices[1],duration=3600):
                    text+=colored('[List   ] Auction set {} {} (OVR:{} NAT:{}) -> sell:min{} max{}; \n'.format(p[0],p[1],p[2],p[3],  prices[0], prices[1]), color='blue')
        result = collections.namedtuple('result',['log', 'requests'])(text,requests)
        return result

    def tradepilemanager(self, tpile): #TODO do czestszego usuwania kupionych
        # players = self.players()
        text = ''
        requests = 0
        i = 0
        #while i < len(tpile):
        resId = tpile['resourceId']
        try:
            p = self.playerinfo(resId)
            if tpile['tradeState'] == 'closed':
                text += colored('[Sold   ] {} {} (OVR:{} NAT:{}) (BID:{} CUR:{} BIN:{} STATUS:{})\n'.format(p[0], p[1], p[2],p[3], tpile['startingBid'],tpile['currentBid'],tpile['buyNowPrice'],tpile['tradeState']),color='green')
                if self.tradepileDelete(tpile['tradeId']):
                    text += colored('[Watcher] Removed sold {} {} (OVR:{} NAT:{}) (BID:{} CUR:{} BIN:{} STATUS:{})\n'.format(p[0], p[1], p[2], p[3], tpile['startingBid'], tpile['currentBid'],tpile['buyNowPrice'], tpile['tradeState']))
        except ValueError:
            # text += colored('[Error  ] It is a consumable or special card\n', color='red')
            pass
        result = collections.namedtuple('result', ['log','requests'])(text, requests)
        return result

    def searchForPlayer(self, assetid, price):#zwraca liste z tym asset id
        auctionsList = self.searchAuctions(ctype='player', assetId=assetid, max_price=price, page_size=16)
        return auctionsList

    def bider(self, tradeid, assetid, bidprice, bidunder=3600):#TODO dodac warunek czasowy jezeli aukcja dluzsza niz np 15 minut to nie biduje
        # print(tradeid, assetid, bidprice)
        asset_id = int(assetid)
        trade_id = int(tradeid)
        bid_price = int(bidprice)
        # print(asset_id)
        # print(type(asset_id))
        p = self.playerinfo(asset_id)
        text = ''
        requests = 0

        if self.checklists is True:  # jezeli jest więcej niz 1 miejsce na wlist i tpile
            if self.bid(trade_id, bid_price, fast=False):
                text += colored('[Bider  ] Biding {} {} (OVR:{} NAT:{}) -> tradeId:{}; \n'.format(p[0], p[1], p[2], p[3], trade_id), color='blue')
                requests += 1
            else:
                text += colored('[Bider  ] Could not bid {} {} (OVR:{} NAT:{}) -> tradeId:{}; \n'.format(p[0], p[1], p[2], p[3], trade_id), color='magenta')
                requests += 1

            self.randomtime(1,3)
        else:
            text += colored('[Watcher] No space on lists skipping biding')
        result = collections.namedtuple('result', ['log', 'requests'])(text, requests)
        return  result



class Addons():
    def sleeppercent(self,sleeptime):
        t = sleeptime/100
        for i in range(101):
            time.sleep(t)
            sys.stdout.write("\r%d%%" % i)
            sys.stdout.flush()

