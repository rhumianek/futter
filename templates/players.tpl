%#template to generate a HTML table from a list of tuples (or list of lists, or tuple of tuples or ...)
%from fut import extras

<p>Players added to bidding list</p>
<table border="1">
<tr><td>asset id</td><td>Player</td><td>max BID</td><td>max BIN</td><td>min sell</td><td>max sell</td><td>FUTBIN PRICE</td><td> </td>
%for row in rows:
%plprice = extras.futbinPrice(row[0], platform='ps')
%delid = row[0]
  <tr>
  %for col in row:
    <td>{{col}}</td>
  %end
  <td>{{plprice}}</td>
  <td> <button onclick="delrow({{delid}})">Delete</button></td>
  </tr>
%end
</table>