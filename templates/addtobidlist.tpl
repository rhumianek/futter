<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Bidding manager</title>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <style>
  .ui-autocomplete-loading {
    background: white url("/usables/ui-anim_basic_16x16.gif") right center no-repeat;
  }
  </style>
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  function delrow( delid ) {
    sentdelid = delid
    $.get( "/bidlist", { delid: sentdelid } )
  .done(function( data ) {
    reloadbiding();
  });
    }

   function addrow( ) {
   sendaddid = $("#assetid").val();
   sendname = $("#players").val();
   sendmaxbid = $("#maxbid").val();
   sendmaxbin = $("#maxbin").val();
   sendsellmin = $("#sellmin").val();
   sendsellmax = $("#sellmax").val();

    $.get( "/bidlist", { addid: sendaddid, name: sendname, maxbid: sendmaxbid, maxbin: sendmaxbin, sellmin: sendsellmin, sellmax: sendsellmax } )
  .done(function( data ) {
    reloadbiding();
  });
    }

  function reloadbiding() {
    $.get( "/players", function( data ) {
    $( "#biddinglist" ).html( data );
 });
    }

  $( function() {

    function log( message ) {
      $( "<div>" ).text( message ).prependTo( "#log" );
      $( "#log" ).scrollTop( 0 );
    }



    function reloadbiding() {
    $.get( "/players", function( data ) {
    $( "#biddinglist" ).html( data );
 });
    }

    $( "#players" ).autocomplete({
      source: "/resources",
      minLength: 2,
      select: function( event, ui ) {
        $("#assetid").val(ui.item.id);
        $("#addrowbtn").val(ui.item.id);
        }
    });

    $.get( "/players", function( data ) {
    $( "#biddinglist" ).html( data );
 });

  } );
  </script>
</head>
<body>

<div class="ui-widget">

  <label for="birds">Players: </label>
  <input id="players" name="players" placeholder = "Start typing name...">
  <input id="assetid" name="asset_id" placeholder = "Asset id" hidden>
  <input id="maxbid" name="maxbid"  placeholder = "Max BID">
  <input id="maxbin" name="maxbin" placeholder = "Max BIN">
  <input id="sellmin" name="sellmin" placeholder = "Sell Min">
  <input id="sellmax" name="sellmax" placeholder = "Sell Max">
  <button id="addrowbtn" name="addrowbtn" onclick="addrow()">Add</button>

</div>



<div id="bidding">
<div id="biddinglist"></div>
</div>


</body>
</html>