#!/bin/bash
remoteuser = "someuser"
#log initialize
echo "Jasper log for: $(date)." > logjasper

# PROD1 sd-862b-a238.nam.nsroot.net
# PROD2 sd-9e8f-62c9.nam.nsroot.net
# PROD3 sd-ccfd-d43f.nam.nsroot.net
# PROD4 sd-84f3-4d59.nam.nsroot.net

declare -a prodservers1 = ("sd-862b-a238.nam.nsroot.net" "sd-9e8f-62c9.nam.nsroot.net" "sd-ccfd-d43f.nam.nsroot.net" "sd-84f3-4d59.nam.nsroot.net")


for server in ${prodservers1[@]}

do
	echo Checking PROD server: $server >> logjasper
	shortdate=$(date '+%m-%d')
	ssh $remoteuser@$server 'cd /export/enterprise-docs/jasper/var/log' >> logjasper
	shortdate=$(ssh $remoteuser@$server 'date '+%m-%d'')
	ssh $remoteuser@$server 'ls -ltr *.log | grep -v "JASPER_RS.log" | grep -v BATCH' >> logjasper
	ssh $remoteuser@$server 'find *.log* -mmin -240 -exec grep "java.lang.NoClassDefFoundError: com/citi/" {}; | wc -l' >> logjasper
	
	ssh $remoteuser@$server 'tail -100 MANTRAN_Messaging.log' | grep ERROR >> logjasper
	ssh $remoteuser@$server 'tail -100 MANTRAN_WEB.log' >> logjasper
	ssh $remoteuser@$server 'tail -100 SUM_WEB.log' >> logjasper
	ssh $remoteuser@$server 'tail -100 Journal_Messaging.log' >> logjasper
	ssh $remoteuser@$server 'tail -100 JASPER_ROUTER.log' >> logjasper
done

# PROD5 sd-796f-fd2e.nam.nsroot.net
# PROD6 sd-e3af-9621.nam.nsroot.net
# PROD7 sd-e213-d5ef.nam.nsroot.net
# PROD8 sd-0be6-1a5d.nam.nsroot.net

prodservers2 = 'sd-796f-fd2e.nam.nsroot.net sd-e3af-9621.nam.nsroot.net sd-e213-d5ef.nam.nsroot.net sd-0be6-1a5d.nam.nsroot.net'

for $server in $prodservers2

do
	echo Checking PROD server: $server
	ssh $remoteuser@$server 'cd /export/enterprise-docs/jasper/var/log'
	ssh $remoteuser@$server 'date'
	ssh $remoteuser@$server 'ls -ltr *.log | grep -v "JASPER_RS.log" | grep -v BATCH'
	ssh $remoteuser@$server 'find *.log* -mmin -240 -exec grep "java.lang.NoClassDefFoundError: com/citi/" {}; | wc -l'
done


# COB1 sd-7bc2-37aa.nam.nsroot.net
# COB2 sd-de36-1d82.nam.nsroot.net
# COB3 sd-a0e4-db2d.nam.nsroot.net
# COB4 sd-392b-bdae.nam.nsroot.net

# Verify COB1 - COB4 servers indicate JASPER modules are NOT running and/or updated for current date.

cobservers1 = 'sd-7bc2-37aa.nam.nsroot.net sd-de36-1d82.nam.nsroot.net sd-a0e4-db2d.nam.nsroot.net sd-392b-bdae.nam.nsroot.net'

for $server in $cobservers1
do
	echo Checking COB server: $server
	ssh $remoteuser@$server 'cd /export/enterprise-docs/jasper/var/log'





# COB5 sd-dc83-0d7c.nam.nsroot.net
# COB6 sd-a196-ca9e.nam.nsroot.net
# COB7 sd-90d3-f7c3.nam.nsroot.net
# COB8 sd-0a5f-6cbe.nam.nsroot.net


cobservers2 = 'sd-dc83-0d7c.nam.nsroot.net sd-a196-ca9e.nam.nsroot.net sd-90d3-f7c3.nam.nsroot.net sd-0a5f-6cbe.nam.nsroot.net'
