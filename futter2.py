from fut import futext
from termcolor import cprint
import json, time, sys, sqlite3

with open('relister_config.json') as data_file:
    futter_config = json.load(data_file)

#settings vars
seting_requests = 450
timeStarted = time.time()
#/settings vars

#while
cprint('[Watcher] Logging in', color='blue')
session = futext.Futext(futter_config["credentials"]["email"], futter_config["credentials"]["passwd"], futter_config["credentials"]["secret_answer"], futter_config["credentials"]["platform"])
futexaddons = futext.Addons()
start_coins = session.credits
requests = seting_requests
do = 1
while do == 1:
    #print(session.relistwithlog)
    if requests < 1:
        elapsedTime = time.time() - timeStarted
        cprint('[DEBUG  ] Time elapsed {} sec'.format(elapsedTime), color='cyan')
        cprint('[Watcher] Logging out', color='blue')
        session.logout()
        if elapsedTime < 3600:
            cooldownTime = (3600 - int(elapsedTime))
            cprint('[Watcher] Cooldown active for {} seconds'.format(int(cooldownTime)), color='blue')
        else:
            cprint('[Watcher] Scheduled cooldown for 10 minutes', color='blue')
            cooldownTime = 600
        futexaddons.sleeppercent(cooldownTime)
        print()
        cprint('[Watcher] Logging in', color='blue')
        timeStarted = time.time(),
        session = futext.Futext(futter_config["credentials"]["email"], futter_config["credentials"]["passwd"], futter_config["credentials"]["secret_answer"], futter_config["credentials"]["platform"])
        requests = seting_requests
        print(session.relistwithlog)

    while requests > 0:
        print(session.printablecredits(startingcredits=start_coins))
        cprint('[Watcher] Checking space on lists')
        if session.checklists is True:
            cprint('[Bider  ] Starting biding')
            conn = sqlite3.connect('usables/trade.db')
            cursor = conn.cursor()
            players = cursor.execute("Select asset_id, maxbid, maxbin, sellmin, sellmax from bidding").fetchall()
            conn.close()
            for player in players:
                searchresults = session.searchForPlayer(player[0],player[1])
                # print(searchresults, player[0], player[1])
                i = 0
                while i < len(searchresults):
                    timeunder = 3600
                    if searchresults[i]['expires'] < timeunder:
                        bidresult = session.bider(searchresults[i]['tradeId'],player[0],player[1], searchresults[i],)
                        print(bidresult.log)
                        requests -= bidresult.requests
                        i += 1
                    else:
                        cprint('[Bider  ] Auction {} longer than {}s'.format(searchresults[i]['tradeId'], timeunder), color='magenta')
        else:
            cprint('[Watcher] No space on lists skipping biding')
            cprint('[Watcher] Waiting for lists for 5 minutes')
            cooldownTime = 300
            futexaddons.sleeppercent(cooldownTime)
            print()

        auctionsWait = session.waitforwatchlist
        requests -= auctionsWait.requests
        waitTime = auctionsWait.waittime
        if waitTime < 90:
            cprint('[Watcher] Waiting {} s for auctions to expire'.format(waitTime))
            print()
            futexaddons.sleeppercent(waitTime)
        wlist = session.watchlist()
        i = 0
        while i < len(wlist):
            result = session.auctioner(wlist[i])
            print(result.log)
            requests -= result.requests
            i += 1
        tpile = session.tradepile()
        i= 0
        while i < len(tpile):
            # print(tpile[i])
            result = session.tradepilemanager(tpile[i])
            if result.log != '':
                print(result.log)
            # requests += result.requests
            i += 1



        requests -= 1
        cprint('[DEBUG  ] requests {}'.format(requests),color='cyan')

        #except Exception as inst:# TODO: except FutError: i ponowny login.
        #cprint('[Error  ] Error occured \n', color='red', on_color='on_yellow')
        # print('[Do     ] logout')
        # time.sleep(600)
        # print('[Do     ] login')
        #print(type(inst))
        #print(inst.args)
        #print(inst)
        # finally:
        #     session.logout()
        #     #cprint('[Error  ] Cleaning up, logging out.\n', color='blue')
        #     do, requests = 0, -1





