from fut import futext
from termcolor import cprint
import json, time, sys

with open('relister_config.json') as data_file:
    futter_config = json.load(data_file)

#settings vars
seting_requests = 450
timeStarted = time.time()
#/settings vars

#while
cprint('[Watcher] Logging in', color='blue')
session = futext.Futext(futter_config["credentials"]["email"], futter_config["credentials"]["passwd"], futter_config["credentials"]["secret_answer"], futter_config["credentials"]["platform"])
futexaddons = futext.Addons()
start_coins = session.credits
requests = seting_requests
do = 1
while do == 1:
    #print(session.relistwithlog)
    if requests < 1:
        elapsedTime = time.time() - timeStarted
        cprint('[DEBUG  ] Time elapsed {} sec'.format(elapsedTime), color='cyan')
        cprint('[Watcher] Logging out', color='blue')
        session.logout()
        if elapsedTime < 3600:
            cooldownTime = (3600 - int(elapsedTime))
            cprint('[Watcher] Cooldown active for {} seconds'.format(int(cooldownTime)), color='blue')
        else:
            cprint('[Watcher] Scheduled cooldown for 10 minutes', color='blue')
            cooldownTime = 600
        futexaddons.sleeppercent(cooldownTime)
        print()
        cprint('[Watcher] Logging in', color='blue')
        timeStarted = time.time(),
        session = futext.Futext(futter_config["credentials"]["email"], futter_config["credentials"]["passwd"], futter_config["credentials"]["secret_answer"], futter_config["credentials"]["platform"])
        requests = seting_requests
        print(session.relistwithlog)

    while requests > 0:
        print(session.printablecredits(startingcredits=start_coins))
        if session.checklists is True:
            cprint('[Bider  ] Starting biding')
            trades = session.silverbiding
            print(trades.log)
            requests -= trades.requests
        else:
            cprint('[Watcher] No space on lists skipping biding')

        auctionsWait = session.waitforwatchlist
        requests -= auctionsWait.requests
        waitTime = auctionsWait.waittime
        if waitTime < 90:
            cprint('[Watcher] Waiting {} s for auctions to expire'.format(waitTime))
            print()
            futexaddons.sleeppercent(waitTime)
        watchlist = session.silverwatchlistmanager
        print(watchlist.log)
        requests -= watchlist.requests
        session.keepalive()
        requests -= 1
        cprint('[DEBUG  ] requests {}'.format(requests),color='cyan')

        #except Exception as inst:# TODO: except FutError: i ponowny login.
        #cprint('[Error  ] Error occured \n', color='red', on_color='on_yellow')
        # print('[Do     ] logout')
        # time.sleep(600)
        # print('[Do     ] login')
        #print(type(inst))
        #print(inst.args)
        #print(inst)
        # finally:
        #     session.logout()
        #     #cprint('[Error  ] Cleaning up, logging out.\n', color='blue')
        #     do, requests = 0, -1





