======
futter
======
futter is a simple terminal FIFA18 UT autobidder/buyer scipt based on oczkers fut api: https://github.com/futapi/fut
fut is a simple library for managing Fifa Ultimate Team.
It is written entirely in Python.




Usage
=====
change _deletethis_relister_config.json to relister_config.json
run futter.py in your console


Bans
^^^^

To avoid getting ban take a look at discussion/guide thread:
https://github.com/oczkers/fut/issues/259

Generally speaking, you should send no more than 500 requests per hour and 5000 requests per day. Be somewhat human. If you encounter a captcha, try to answer/solve it as soon as possible.



License
-------

GNU GPLv3
